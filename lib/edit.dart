import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'student.dart';

class Edit extends StatefulWidget {
  final Student student;

  Edit({required this.student});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();

  // Http post request
  Future editStudent() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/flutter_api/update.php"),
      body: {
        "id": widget.student.id.toString(),
        "name": nameController.text,
        "age": ageController.text
      },
    );
  }

  void _onConfirm(context) async {
    await editStudent();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.student.name);
    ageController = TextEditingController(text: widget.student.age.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit student"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: "Student Name:",
                hintText: "Enter student name",
              ),
            )),
            Container(
                child: TextField(
              controller: ageController,
              decoration: InputDecoration(
                labelText: "Age:",
                hintText: "Enter product age",
              ),
            )),
          ],
        ),
      ),
    );
  }
}
